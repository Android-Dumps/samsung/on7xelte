## on7xeltedd-user 8.1.0 M1AJQ G610FDXS1CTE2 release-keys
- Manufacturer: samsung
- Platform: exynos5
- Codename: on7xelte
- Brand: samsung
- Flavor: on7xeltedd-user
- Release Version: 8.1.0
- Id: M1AJQ
- Incremental: G610FDXS1CTE2
- Tags: release-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 480
- Fingerprint: samsung/on7xeltedd/on7xelte:8.1.0/M1AJQ/G610FDXS1CTE2:user/release-keys
- OTA version: 
- Branch: on7xeltedd-user-8.1.0-M1AJQ-G610FDXS1CTE2-release-keys
- Repo: samsung/on7xelte
